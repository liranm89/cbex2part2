package mainPackage;

import java.util.ArrayList;
/**
 * This is the selection class - it is responsible for the elitism
 */
public class Selection {
	
	protected int size;
	/**
	 * Selection constructor
	 * @param size size of individuals we want to replace
	 */
	public Selection (int size) {this.size = size;}
	
	/**
	 * Creating a new population
	 * @param population old population
	 * @param offsprings new population
	 */
	public void newPopulation(ArrayList<Individual> population, ArrayList<Individual> offsprings) {
		population.sort(new PopulationSorter());
		int k = 0;
		//Replace this.size individuals
		for (int i = population.size() - 1; i >= population.size() - this.size; i--) {
			population.set(i, offsprings.get(k++));
		}	
	}
	
	/**
	 * @param size the size of individuals we want to replace
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * @return the size of individuals we want to replace
	 */
	public int getSize() {
		return this.size;
	}
	
	/**
	 * For personal use
	 */
	public String toString() {
		return this.getClass().getSimpleName() + "\t with removing " + this.size + " each round";
	}
}
