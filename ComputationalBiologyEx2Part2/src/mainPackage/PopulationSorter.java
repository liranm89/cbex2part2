package mainPackage;
import java.util.Comparator;
/**
 * Population sorter
 */
public class PopulationSorter implements Comparator<Individual> {
	@Override
	public int compare(Individual o1, Individual o2) {
		return o1.getScore() < o2.getScore() ? 1 : o1.getScore() == o2.getScore() ? 0 : -1;
	}
}
