package mainPackage;

import problem.NonLinearEquationRealProblem;
/**
 * This is the individual class. 
 */
public class Individual {
	//Different DNAs
	private String IntegerDNA;
	private String realDNA;
	private double score;
	/**
	 * Individual constructor
	 * @param DNA The DNA of the individual
	 */
	public Individual(String IntegerDNA, String realDNA) {
		setIntegerDNA(IntegerDNA);
		setRealDNA(realDNA);
	}
	/**
	 * Copy constructor
	 * @param individual an Individual which we want to copy from
	 */
	public Individual(Individual individual) {
		setIntegerDNA(new String(individual.IntegerDNA));
		setRealDNA(new String(individual.realDNA));
	}
	
	/**
	 * @param score The fitness of the individual
	 */
	public void setScore(double score) {
		this.score = score;
	}
	
	/**
	 * @return its score (fitness)
	 */
	public double getScore() {
		return this.score;
	}
	
	/**
	 * For personal use
	 */
	public String toString() {
		double[] variables = NonLinearEquationRealProblem.getVariablesValues(this);
		String solution = "[";
		for (int i = 0; i < variables.length; i++) {
			solution += variables[i] + ",";
		}
		solution = solution.substring(0, solution.length() - 1);
		solution += "]";
		return "DNA:\t" + this.IntegerDNA + "|" + this.realDNA + "\tScore:\t" + this.score + "\n" + solution;
	}
	
	/**
	 * @return DNA of Integers
	 */
	public String getIntegerDNA() {
		return IntegerDNA;
	}
	
	/**
	 * @param integerDNA DNA of Integers
	 */
	public void setIntegerDNA(String integerDNA) {
		IntegerDNA = integerDNA;
	}
	
	/**
	 * @return DNA of real numbers
	 */
	public String getRealDNA() {
		return realDNA;
	}
	
	/**
	 * @param realDNA DNA of real numbers
	 */
	public void setRealDNA(String realDNA) {
		this.realDNA = realDNA;
	}
}
