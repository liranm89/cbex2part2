package problem;
import java.util.ArrayList;
import mainPackage.Individual;

/**
 * Problem Super Class.
 * derived classes are different problems
 */
public abstract class Problem {
	protected int individualLength;
	/**
	 * Problem constructor
	 * @param individualLength the length of the individual
	 */
	public Problem (int individualLength) {
		this.individualLength = individualLength;
	}
	/**
	 * This method initializes the population
	 * @param population the population we want to initialize
	 * @param sizeOfPopulation the size of the wanted population
	 */
	public abstract void initialize(ArrayList<Individual> population, int sizeOfPopulation);
	
	/**
	 * This method initializes the database
	 * @param fileName name of the file
	 */
	public abstract void initializeDatabase(String fileName);
	
	/**
	 * this method calculates the fitness of an individual
	 * @param individual The individual
	 * @return its fitness
	 */
	public abstract double individualFitness(Individual individual);
	//
	/**
	 * this method calculates the fitness for all population
	 * @param population the population
	 */
	public void calculateFitness (ArrayList<Individual> population) {
		//For each individual
		for (int i = 0; i < population.size(); i++) {
			population.get(i).setScore(individualFitness(population.get(i)));
		}
	}
	
	/**
	 * The population learn in this function
	 * @param population
	 * @param real to know which learning to make
	 */
	public void learn(ArrayList<Individual> population, boolean real) {
		for (int i = 0; i < population.size(); i++) {
			//For each individual
			//specific learn
			Individual trainedIndividual = null;
			//just change the .xxx numbers
			if (real) {
				trainedIndividual = this.individualLearn2(population.get(i));
			} else {
				trainedIndividual = this.individualLearn(population.get(i));
			}
			
			trainedIndividual.setScore(individualFitness(trainedIndividual));
			//in case of a better result, update the individual
			if (trainedIndividual.getScore() >= population.get(i).getScore()) {
				population.set(i, trainedIndividual);
			} 
		}
	}
	/**
	 * This method tries to make the individual better by learning.
	 * Learning is as follows:
	 * Take the fraction digits DNA and mutate it a couple of times. Choose the best option
	 */
	protected abstract Individual individualLearn2(Individual individual);
	/**
	 * @param num number in base 10
	 * @param numOfBits number of bits wanted
	 * @return base 2 representation
	 */
	protected abstract Individual individualLearn(Individual individual);
}
