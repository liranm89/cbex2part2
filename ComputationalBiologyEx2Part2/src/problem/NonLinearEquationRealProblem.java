package problem;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import mainPackage.Database;
import mainPackage.FileWriterAndReader;
import mainPackage.Individual;
import mainPackage.Main;
import mainPackage.PopulationSorter;
/**
 * Derived class of Problem,
 * Implements the non linear equations problem
 */
public class NonLinearEquationRealProblem extends Problem {
	private static final int FRACTION_DIGIT_MUTATION_NUMBER = 7;
	private static final char MINUS_BIT = '1';
	private static final char PLUS_BIT = '0';
	private int individualRealLength;
	private Database database = null;
	private ArrayList<String> options = new ArrayList<String>();

	/**
	 * Constructor
	 * @param individualLength the length of an individual
	 */
	public NonLinearEquationRealProblem(int individualIntLength, int individualRealLength) {
		super(individualIntLength);
		setIndividualRealLength(individualRealLength);
		//We will use this for the learning type
		fillOptions("", Main.NUMBER_OF_VARIABLES);
	}

	/**
	 * This method initializes the population
	 * @param population the population we want to initialize
	 * @param sizeOfPopulation the size of the wanted population
	 */
	@Override
	public void initialize(ArrayList<Individual> population, int sizeOfPopulation) {
		Random rand = new Random();
		//For each individual
		for (int i = 0; i < sizeOfPopulation; i++) {
			String intBitsString = "";
			//Create random bits for the Integer DNA
			for (int j = 0; j < this.individualLength; j++) {
				//Create random bits
				intBitsString += Integer.toString(rand.nextInt(2));
			}
			String realBitsString = "";
			//Create random bits for the real DNA
			for (int j = 0; j < this.individualRealLength; j++) {
				realBitsString += Integer.toString(rand.nextInt(2));
			}
			Individual individual = new Individual(intBitsString, realBitsString);
			population.add(individual);
		}

	}

	/**
	 * This method initializes the database
	 * @param fileName name of the file
	 */
	@Override
	public void initializeDatabase(String fileName) {
		try {
			this.database = new FileWriterAndReader().readFromFile(Main.FILENAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * this method calculates the fitness of an individual
	 * @param individual The individual
	 * @return its fitness
	 */
	@Override
	public double individualFitness(Individual individual) {
		//Get variables array
		double[] variables = getVariablesValues(individual);
		double answer = 0;
		double sum = 0;
		double subtraction;
		//for each equation
		double epsilon = 0.1;
		for (int i = 0; i < Main.NUMBER_OF_EQUATIONS; i++) {
			//Get the value of the left side of the equation
			answer = getScoreFromEquation(i, variables);
			subtraction = Math.abs(this.database.getSolutions()[i] - answer);
			if (subtraction <= epsilon) {
				subtraction = 0;
			}
			//Sum up the absolute value of the solution(right side) - the answer from above
			sum += subtraction;
		} 
		//If it's 0, we got the answer! Otherwise, 1/log(sum)
		//Reason for log: Because we want the fitness values to be closer to each other
		//Reason for 1/sum: We want the best individuals to have higher fitness
		//We need to add 1 because we do ln on it, therefore this normalization
		sum += 1;
		return sum == 1 ? java.lang.Integer.MAX_VALUE : 1 / Math.log(sum);
	}

	/**
	 * this method gets the variables values from its bits
	 * @param individual The individual
	 * @return its variables values
	 */
	public static double[] getVariablesValues(Individual individual) {
		String integerDNA = individual.getIntegerDNA();
		String realDNA = individual.getRealDNA();
		double[] variables = new double[Main.NUMBER_OF_VARIABLES];
		int intIndex = 0, realIndex = 0;

		//3 fraction digits
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(3);
		//end test

		//For each variable
		for (int i = 0; i < variables.length; i++) {
			//get its value from the bits
			double num;
			//		num = bitsToInt(integerDNA.substring(intIndex, intIndex + Main.BITS_LENGTH)) +
			//				bitsToReal(realDNA.substring(realIndex, realIndex + Main.REAL_BITS_LENGTH));
			num = bitsToInt(integerDNA.substring(intIndex, intIndex + Main.BITS_LENGTH)) +
					Double.parseDouble(
							df.format(bitsToReal(realDNA.substring(realIndex, realIndex + Main.REAL_BITS_LENGTH)))
							);
			variables[i] = num;
			intIndex += Main.BITS_LENGTH;
			realIndex += Main.REAL_BITS_LENGTH;
		}
		return variables;
	}

	/**
	 * This function gets the score from an equation
	 * @param i equation number
	 * @param variables the values of the variables
	 * @return
	 */
	public double getScoreFromEquation(int i, double[] variables) {
		double sum = 0;
		double multiplication = 1;
		int[][][] equations = this.database.getEquationsMatrix();
		//For each addition
		for (int j = 0; j < equations[i].length; j++) {
			//Multiply
			multiplication *= equations[i][j][0];
			//If it's 0, then just continue to the next multiplication
			if (multiplication == 0) {
				continue;
			}
			for (int k = 1; k < equations[i][j].length; k++) {
				//We want to avoid 0^0
				if (equations[i][j][k] != 0) {
					multiplication *=  Math.pow(variables[k - 1], equations[i][j][k]);
				}
			}
			sum += multiplication;
			multiplication = 1;
		}
		return sum;
	}

	/**
	 * @param bits
	 * @return base 10
	 */
	public static int bitsToInt(String bits) {
		int num = Integer.parseInt(bits.substring(1), 2);
		return bits.charAt(0) == MINUS_BIT ? -1 * num : num;
	}
	
	/**
	 * @param bits
	 * @return fraction digits
	 */
	public static double bitsToReal(String bits) {
		double num = 0;
		for (int i = 1; i < bits.length(); i++) {
			num += Character.getNumericValue(bits.charAt(i)) * (Math.pow(2, -1 * i));
		}
		return bits.charAt(0) == MINUS_BIT ? -1 * num : num;
	} 
	
	/**
	 * @return the individual length
	 */
	public int getIndividualRealLength() {
		return individualRealLength;
	}
	
	/**
	 * @param individualRealLength the individual length
	 */
	public void setIndividualRealLength(int individualRealLength) {
		this.individualRealLength = individualRealLength;
	}

	/**
	 * This method tries to make the individual better by learning.
	 * Learning is as follows:
	 * X1 +- 1, X2 +- 1, X3 +- 1
	 * There are 2^variables variations. Choose the best one
	 */
	@Override
	protected Individual individualLearn(Individual individual) {
		//Get it's variables
		double[] variablesDouble = getVariablesValues(individual);
		//Get their int values
		int[] variables = new int[variablesDouble.length];
		for (int i = 0; i < variables.length; i++) {
			variables[i] = (int) variablesDouble[i];
		}
		ArrayList<Individual> listOfChangedIndividuals = new ArrayList<Individual>();
		//Create variations of +- the number
		int[][] variations = new int[options.size()][variables.length];
		char[] DNA = individual.getIntegerDNA().toCharArray();
		for (int i = 0; i < variations.length; i++) {
			for (int j = 0; j < variations[i].length; j++) {
				if (options.get(i).charAt(j) == '1') {
					variations[i][j] = variables[j] + 1;
				} else  if (options.get(i).charAt(j) == '0'){
					variations[i][j] = variables[j] - 1;
				}
			}
		}
		//Convert to bits
		for (int i = 0; i < variations.length; i++) {
			String bits = new String("");
			for (int j = 0; j < variations[i].length; j++) {
				if (variations[i][j] < 0) {
					bits += MINUS_BIT;
				} else {
					bits += PLUS_BIT;
				}
				bits += intToBits(variations[i][j], Main.BITS_LENGTH - 1);
			}
			Individual changedIndividual = new Individual(bits, individual.getRealDNA());
			changedIndividual.setScore(this.individualFitness(changedIndividual));
			listOfChangedIndividuals.add(changedIndividual);
		}
		//Sort and return the best one
		listOfChangedIndividuals.sort(new PopulationSorter());
		return listOfChangedIndividuals.get(0);
	}
	
	/**
	 * This method tries to make the individual better by learning.
	 * Learning is as follows:
	 * Take the fraction digits DNA and mutate it a couple of times. Choose the best option
	 */
	@Override
	protected Individual individualLearn2(Individual individual) {
		ArrayList<Individual> listOfChangedIndividuals = new ArrayList<Individual>();
		//Get a random number
		Random rand = new Random();
		for (int i = 0; i < FRACTION_DIGIT_MUTATION_NUMBER; i++) {
			Individual trainedIndividual = new Individual(individual);
			//copy to char array in order to change
			char[] dnaCharArray = trainedIndividual.getRealDNA().toCharArray();
			int index = rand.nextInt(trainedIndividual.getRealDNA().length());
			//Mutate 
			if (dnaCharArray[index] == '0') {
				dnaCharArray[index] = '1';
			} else {
				dnaCharArray[index] = '0';
			} 
			trainedIndividual.setRealDNA(String.valueOf(dnaCharArray));
			trainedIndividual.setScore(this.individualFitness(trainedIndividual));
			listOfChangedIndividuals.add(trainedIndividual);
		}
		//Sort and get the best one
		listOfChangedIndividuals.sort(new PopulationSorter());
		return listOfChangedIndividuals.get(0);
	}
	
	/**
	 * @param num number in base 10
	 * @param numOfBits number of bits wanted
	 * @return base 2 representation
	 */
	public static String intToBits(int num, int numOfBits) {
		String bits = "";
		for(int i = 0; i < numOfBits; i++) {
			if (num % 2 == 0) {
				bits = "0" + bits;
			} else {
				bits = "1" + bits;
			}
			num = num / 2;
		}
		return bits;
	}
	
	/**
	 * fill options to get all variations (this is for the learning part)
	 * @param str current string
	 * @param length wanted length
	 */
	private void fillOptions(String str, int length) {
		if (str.length() == length) {
			this.options.add(str);
			return;
		} 
		fillOptions(str + "1",length);
		fillOptions(str + "0",length);
	}



}
